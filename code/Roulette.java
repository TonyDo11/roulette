import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int userMoney = 1000;
        boolean playAgain = true;

        while (playAgain) {
            System.out.println("You currently have $" + userMoney);
            System.out.print("Do you want to make a bet? (yes/no): ");
            String choice = scan.nextLine();

            if (choice.equalsIgnoreCase("yes")) {
                System.out.print("Enter the amount you want to bet: ");
                int betAmount = scan.nextInt();

                if (betAmount > userMoney) {
                    System.out.println("Sorry, you don't have enough money for that bet.");
                    continue;
                }

                System.out.print("Enter the number you want to bet on (0-36): ");
                int betNumber = scan.nextInt();

                RouletteWheel rouletteWheel = new RouletteWheel();
                rouletteWheel.spin();
                int spinResult = rouletteWheel.getValue();

                System.out.println("The wheel landed on: " + spinResult);

                if (spinResult == betNumber) {
                    int winnings = betAmount * 35;
                    userMoney += winnings;
                    System.out.println("Congratulations! You won $" + winnings);
                } else {
                    userMoney -= betAmount;
                    System.out.println("Sorry! You lost $" + betAmount);
                }
            } else if (choice.equals("no")) {
                playAgain = false;
            } else {
                System.out.println("Invalid choice. Please enter 'yes' or 'no'.");
            }
        }

        System.out.println("Thanks for playing! Your final balance: $" + userMoney);
    }
}
